import sys
import queue

# CONSTANTS GO HERE

R = 0
C = 0
F = 0
N = 0
B = 0
T = 0

# GLOBALS HERE

Time = 0

# CLASS DEFINITIONS HERE


class Ride:
    def __init__(self, line):
        vals = line.split(" ")
        self.startInt = (int(vals[0]), int(vals[1]))
        self.endInt = (int(vals[2]), int(vals[3]))
        self.startTime, self.finishTime = int(vals[4]), int(vals[5])
        self.deadline = self.finishTime - taxiDist(self.endInt, self.startInt) - 1
        self.realFinish = 0
        self.length = taxiDist(self.endInt, self.startInt)

    def __str__(self):
        return "STARTINT : {0} | ENDINT : {1} | STARTTIME : {2} | ENDTIME : {3}".format(self.startInt, self.endInt, self.startTime, self.finishTime)


class Taxi:
    def __init__(self):
        self.pos = (0, 0)
        self.free = True
        self.avail = 0
        self.rides = []
        self.rides.append(Ride("0 0 0 0 0 0"))

    def feasible(self, ride):
        global Time
        return ride.deadline - Time >= (taxiDist(ride.startInt, self.pos))

    def __cmp__(self, other):
        cmp = self.avail - other.avail
        return 1 if cmp > 0 else -1 if cmp < 0 else 1

    def __lt__(self, other):
        return self.avail < other.avail

# OTHER PROTOTYPES HERE


def taxiDist(a, b):
    return abs(a[0] - b[0])  + abs(a[1] - b[1])

def rideSorter(taxi, ride):
    bonusScore = abs(ride.startTime - taxi.rides[-1].realFinish - taxiDist(taxi.rides[-1].endInt, ride.startInt))
    dstScore = - taxiDist(ride.endInt, ride.startInt)
    return (bonusScore, dstScore)

# MAIN CODE HERE

if (len(sys.argv) < 2):
    print("Usage: {0} <inputFile>".format(sys.argv[0]))
    exit(0)

inputFile = open(sys.argv[1])
R, C, F, N, B, T = [int(c) for c in inputFile.readline()[:-1].split(" ")]
print("R:{0} C:{1} F:{2} N:{3} B:{4} T:{5}".format(R, C, F, N, B, T))

rides = [Ride(l) for l in inputFile.readlines()]

for ride in rides:
    print(ride)

taxis = [Taxi() for _ in range(F)]
taxiQueue = queue.PriorityQueue()

for taxi in taxis:
    taxiQueue.put(taxi)

while not taxiQueue.empty():
    taxi = taxiQueue.get()
    Time = taxi.avail
    if Time > T:
        break

    filteredRides = [ride for ride in rides if taxi.feasible(ride)]

    if len(filteredRides) == 0:
        continue

    rideQueue = queue.PriorityQueue()
    for ride in filteredRides:
        rideQueue.put((rideSorter(taxi, ride), ride))

    bestRide = rideQueue.get()[1]
    rides.remove(bestRide)
    taxi.rides.append(bestRide)
    taxi.avail = taxiDist(taxi.pos, bestRide.startInt) + bestRide.length
    bestRide.realFinish = taxi.avail
    taxi.pos = bestRide.endInt
    taxiQueue.put(taxi)

i = 0
for taxi in taxis:
    print("TAXI: {0}".format(i))
    i = i + 1
    for ride in taxi.rides[1:]:
        print(ride)
